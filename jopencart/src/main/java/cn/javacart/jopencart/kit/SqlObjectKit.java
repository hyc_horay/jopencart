package cn.javacart.jopencart.kit;

import java.util.List;
import java.util.Map;

import com.jfinal.kit.StrKit;

/**
 * SQL管理中的对象相关工具
 * @author farmer
 *
 */
public class SqlObjectKit {

	/**
	 * 判断是否为空
	 * @param id
	 * 	对象
	 * @return
	 */
	public static boolean notBlank(Object id){
		if(id == null){
			return false;
		}
		if(id instanceof String){
			return StrKit.notBlank((String)id);
		}
		if(id instanceof List){
			return ((List<?>) id).size() != 0;
		}
		if(id instanceof Map){
			return ((Map<?,?>) id).size() != 0;
		}
		if(id instanceof Object[]){
			return ((Object[]) id).length != 0;
		}
		return true;
	}
	
	/**
	 * 判断为空
	 * @param id
	 */
	public static boolean isBlank(Object id){
		return !notBlank(id);
	}
	
	/**
	 * 判断是否为集合
	 * @param id
	 * @return
	 */
	public static boolean isArray(Object id){
		if(id == null){
			return false;
		}
		if(id instanceof List){
			return true;
		}
		if(id instanceof Map){
			return true;
		}
		if(id instanceof Object[]){
			return true;
		}
		return false;
	}
	
}
