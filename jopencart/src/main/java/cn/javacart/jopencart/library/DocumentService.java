package cn.javacart.jopencart.library;

import java.util.HashMap;
import java.util.Map;

/**
 * 文档处理服务
 * @author farmer
 *
 */
public class DocumentService {

	/**
	 * 标题
	 */
	private String title;
	/**
	 * 描述
	 */
	private String description;
	/**
	 * 关键字
	 */
	private String keywords;
	/**
	 * 链接
	 */
	private Map<String,Map<String, String>> links = new HashMap<String, Map<String,String>>();
	/**
	 * 样式
	 */
	private Map<String,Map<String, String>> styles = new HashMap<String, Map<String,String>>();
	/**
	 * 脚本
	 */
	private Map<String,Map<String, String>> scripts = new HashMap<String, Map<String,String>>();
	
	/**
	 * 线程变量
	 */
	static ThreadLocal<DocumentService> documentLocal = new ThreadLocal<DocumentService>();

	/**
	 * 重置
	 */
	public static void reset(){
		documentLocal.set(null);
	}
	
	public static DocumentService getThreadInstance() {
		DocumentService document = documentLocal.get();
		if(document == null){
			document = new DocumentService();
			documentLocal.set(document);
		}
		return document;
	}
	
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public String getKeywords() {
		return keywords;
	}
	public void setKeywords(String keywords) {
		this.keywords = keywords;
	}

	/**
	 * 添加链接
	 * @param href
	 * @param rel
	 */
	public void addLink(String href, String rel) {
		Map<String, String> link = new HashMap<String, String>();
		link.put("href", href);
		link.put("rel", rel);
		links.put(href, link);
	}
	
	/**
	 * 添加样式
	 * @param href
	 * @param rel
	 * @param media
	 */
	public void addStyle(String href, String rel /* = 'stylesheet'*/, String media /*= 'screen'*/) {
		rel = rel == null ? "stylesheet":rel;
		media = media == null ? "screen":media;
		Map<String, String> style = new HashMap<String, String>();
		style.put("href", href);
		style.put("rel", rel);
		style.put("media", media);
		styles.put(href,style);
	}
	
	/**
	 * 添加脚本
	 * @param href
	 * 脚本
	 * @param postion
	 * 位置
	 */
	public void addScript(String href, String postion) {
		postion = postion == null ? "header":postion;
		Map<String, String> postionScript = scripts.get(postion);
		if(postionScript == null){
			postionScript =  new HashMap<String, String>();
		}
		postionScript.put(href, href);
		scripts.put(postion, postionScript);
	}

	/**
	 * 获取Links
	 * @return
	 */
	public Map<String, Map<String, String>> getLinks() {
		return links;
	}

	/**
	 * 获取样式
	 * @return
	 */
	public Map<String, Map<String, String>> getStyles() {
		return styles;
	}

	/**
	 * 获取脚本
	 * @param postion
	 * 位置
	 * @return
	 */
	public Map<String, String> getScripts(String postion) {
		postion = postion == null ? "header":postion;
		Map<String, String> postionScript = scripts.get(postion);
		if(postionScript == null){
			postionScript =  new HashMap<String, String>();
		}
		return postionScript;
	}
}