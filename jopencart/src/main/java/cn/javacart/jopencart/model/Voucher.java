/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_voucher表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|voucher_id          |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|code                |VARCHAR(10)         |false |false   |NULL    |
|from_name           |VARCHAR(64)         |false |false   |NULL    |
|from_email          |VARCHAR(96)         |false |false   |NULL    |
|to_name             |VARCHAR(64)         |false |false   |NULL    |
|to_email            |VARCHAR(96)         |false |false   |NULL    |
|voucher_theme_id    |INT(10)             |false |false   |NULL    |
|message             |TEXT(65535)         |false |false   |NULL    |
|amount              |DECIMAL(15)         |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Voucher extends Model<Voucher>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2489231483282248923L;
	/**
	 * 用于查询操作
	 */
	public static final Voucher ME = new Voucher();
	
}