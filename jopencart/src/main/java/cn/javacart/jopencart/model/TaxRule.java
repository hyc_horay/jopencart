/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_tax_rule表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|tax_rule_id         |INT(10)             |false |true    |NULL    |
|tax_class_id        |INT(10)             |false |false   |NULL    |
|tax_rate_id         |INT(10)             |false |false   |NULL    |
|based               |VARCHAR(10)         |false |false   |NULL    |
|priority            |INT(10)             |false |false   |1|
+--------------------+--------------------+------+--------+--------+--------

 */
public class TaxRule extends Model<TaxRule>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2482931483282248293L;
	/**
	 * 用于查询操作
	 */
	public static final TaxRule ME = new TaxRule();
	
}