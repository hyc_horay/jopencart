/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_filter表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|filter_id           |INT(10)             |false |true    |NULL    |
|filter_group_id     |INT(10)             |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Filter extends Model<Filter>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2397871483282239787L;
	/**
	 * 用于查询操作
	 */
	public static final Filter ME = new Filter();
	
}