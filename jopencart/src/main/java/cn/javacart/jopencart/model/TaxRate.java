/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_tax_rate表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|tax_rate_id         |INT(10)             |false |true    |NULL    |
|geo_zone_id         |INT(10)             |false |false   |0|
|name                |VARCHAR(32)         |false |false   |NULL    |
|rate                |DECIMAL(15)         |false |false   |0.0000|
|type                |CHAR(1)             |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class TaxRate extends Model<TaxRate>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2480401483282248040L;
	/**
	 * 用于查询操作
	 */
	public static final TaxRate ME = new TaxRate();
	
}