/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_order_total表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|order_total_id      |INT(10)             |false |true    |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|code                |VARCHAR(32)         |false |false   |NULL    |
|title               |VARCHAR(255)        |false |false   |NULL    |
|value               |DECIMAL(15)         |false |false   |0.0000|
|sort_order          |INT(10)             |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class OrderTotal extends Model<OrderTotal>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2441101483282244110L;
	/**
	 * 用于查询操作
	 */
	public static final OrderTotal ME = new OrderTotal();
	
}