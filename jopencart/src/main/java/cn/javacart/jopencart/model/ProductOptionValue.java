/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_option_value表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_option_value_id|INT(10)             |false |true    |NULL    |
|product_option_id   |INT(10)             |false |false   |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|option_id           |INT(10)             |false |false   |NULL    |
|option_value_id     |INT(10)             |false |false   |NULL    |
|quantity            |INT(10)             |false |false   |NULL    |
|subtract            |BIT(0)              |false |false   |NULL    |
|price               |DECIMAL(15)         |false |false   |NULL    |
|price_prefix        |VARCHAR(1)          |false |false   |NULL    |
|points              |INT(10)             |false |false   |NULL    |
|points_prefix       |VARCHAR(1)          |false |false   |NULL    |
|weight              |DECIMAL(15)         |false |false   |NULL    |
|weight_prefix       |VARCHAR(1)          |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductOptionValue extends Model<ProductOptionValue>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2453001483282245300L;
	/**
	 * 用于查询操作
	 */
	public static final ProductOptionValue ME = new ProductOptionValue();
	
}