/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_banner_image表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|banner_image_id     |INT(10)             |false |true    |NULL    |
|banner_id           |INT(10)             |false |false   |NULL    |
|link                |VARCHAR(255)        |false |false   |NULL    |
|image               |VARCHAR(255)        |false |false   |NULL    |
|sort_order          |INT(10)             |false |false   |0|
+--------------------+--------------------+------+--------+--------+--------

 */
public class BannerImage extends Model<BannerImage>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2350211483282235021L;
	/**
	 * 用于查询操作
	 */
	public static final BannerImage ME = new BannerImage();
	
}