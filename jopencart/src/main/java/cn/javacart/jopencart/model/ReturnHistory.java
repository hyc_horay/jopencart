/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_return_history表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|return_history_id   |INT(10)             |false |true    |NULL    |
|return_id           |INT(10)             |false |false   |NULL    |
|return_status_id    |INT(10)             |false |false   |NULL    |
|notify              |BIT(0)              |false |false   |NULL    |
|comment             |TEXT(65535)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ReturnHistory extends Model<ReturnHistory>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2470161483282247016L;
	/**
	 * 用于查询操作
	 */
	public static final ReturnHistory ME = new ReturnHistory();
	
}