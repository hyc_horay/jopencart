/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_setting表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|setting_id          |INT(10)             |false |true    |NULL    |
|store_id            |INT(10)             |false |false   |0|
|code                |VARCHAR(32)         |false |false   |NULL    |
|key                 |VARCHAR(64)         |false |false   |NULL    |
|value               |TEXT(65535)         |false |false   |NULL    |
|serialized          |BIT(0)              |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Setting extends Model<Setting>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2475391483282247539L;
	/**
	 * 用于查询操作
	 */
	public static final Setting ME = new Setting();
	
}