/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_filter_description表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|filter_id           |INT(10)             |false |true    |NULL    |
|language_id         |INT(10)             |false |true    |NULL    |
|filter_group_id     |INT(10)             |false |false   |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class FilterDescription extends Model<FilterDescription>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2398981483282239898L;
	/**
	 * 用于查询操作
	 */
	public static final FilterDescription ME = new FilterDescription();
	
}