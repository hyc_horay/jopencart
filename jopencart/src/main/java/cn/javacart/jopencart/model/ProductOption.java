/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_product_option表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|product_option_id   |INT(10)             |false |true    |NULL    |
|product_id          |INT(10)             |false |false   |NULL    |
|option_id           |INT(10)             |false |false   |NULL    |
|value               |TEXT(65535)         |false |false   |NULL    |
|required            |BIT(0)              |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class ProductOption extends Model<ProductOption>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2451661483282245166L;
	/**
	 * 用于查询操作
	 */
	public static final ProductOption ME = new ProductOption();
	
}