/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_store表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|store_id            |INT(10)             |false |true    |NULL    |
|name                |VARCHAR(64)         |false |false   |NULL    |
|url                 |VARCHAR(255)        |false |false   |NULL    |
|ssl                 |VARCHAR(255)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Store extends Model<Store>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2477811483282247781L;
	/**
	 * 用于查询操作
	 */
	public static final Store ME = new Store();
	
}