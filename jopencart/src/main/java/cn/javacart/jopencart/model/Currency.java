/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_currency表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|currency_id         |INT(10)             |false |true    |NULL    |
|title               |VARCHAR(32)         |false |false   |NULL    |
|code                |VARCHAR(3)          |false |false   |NULL    |
|symbol_left         |VARCHAR(12)         |false |false   |NULL    |
|symbol_right        |VARCHAR(12)         |false |false   |NULL    |
|decimal_place       |CHAR(1)             |false |false   |NULL    |
|value               |FLOAT(15)           |false |false   |NULL    |
|status              |BIT(0)              |false |false   |NULL    |
|date_modified       |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class Currency extends Model<Currency>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2370581483282237058L;
	/**
	 * 用于查询操作
	 */
	public static final Currency ME = new Currency();
	
}