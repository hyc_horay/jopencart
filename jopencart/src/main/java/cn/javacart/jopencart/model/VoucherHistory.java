/**
 * Copyright (c) 20152015 - 2017, 席有芳 (951868171@qq.com).
 *
 */

package cn.javacart.jopencart.model;

import com.jfinal.plugin.activerecord.Model;

/*
 * @author 席有芳
 * Powered By 超前端科技
 * Web Site: http://www.yfore.com
 * createDate: 2017-1-1
 * Since 2015 - 2017
 */

/**
 * 
joc_voucher_history表结构
+--------------------+--------------------+------+--------+--------+--------
|Field               |Type                |Null  |Primary |Default |remarks                     
+--------------------+--------------------+------+--------+--------+--------
|voucher_history_id  |INT(10)             |false |true    |NULL    |
|voucher_id          |INT(10)             |false |false   |NULL    |
|order_id            |INT(10)             |false |false   |NULL    |
|amount              |DECIMAL(15)         |false |false   |NULL    |
|date_added          |DATETIME(19)        |false |false   |NULL    |
+--------------------+--------------------+------+--------+--------+--------

 */
public class VoucherHistory extends Model<VoucherHistory>{

	/**
	 * 序列化
	 */
	private static final long serialVersionUID = 2490501483282249051L;
	/**
	 * 用于查询操作
	 */
	public static final VoucherHistory ME = new VoucherHistory();
	
}