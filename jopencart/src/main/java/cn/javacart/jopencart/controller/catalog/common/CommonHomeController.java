package cn.javacart.jopencart.controller.catalog.common;

import com.jfinal.kit.StrKit;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;

/**
 * 
 * @author farmer
 *
 */
public class CommonHomeController extends JOpencartController{
	
	public void index(){
		/*设置页面相关信息*/
		document.setTitle((String)config.get("config_meta_title"));
		document.setDescription((String)config.get("config_meta_description"));
		document.setKeywords((String)config.get("config_meta_keyword"));
		if (StrKit.notBlank(route)) {document.addLink("", "canonical");}
		/*加载页面组件*/
		dataMap.put("column_left", MemoryUtils.valueOf(load.module(new CommonColumnLeftModule(this))));
		dataMap.put("column_right", MemoryUtils.valueOf(load.module(new CommonColumnRightModule(this))));
		dataMap.put("content_top", MemoryUtils.valueOf(load.module(new CommonContentTopModule(this))));
		dataMap.put("content_bottom", MemoryUtils.valueOf(load.module(new CommonContentBottomModule(this))));
		dataMap.put("footer", MemoryUtils.valueOf(load.module(new CommonFooterModule(this))));
		dataMap.put("header", MemoryUtils.valueOf(load.module(new CommonHeaderModule(this))));
		/*渲染主页面*/
		render(new PhpRender("/catalog/view/theme/default/template/common/home.tpl", dataMap));
	}
}
