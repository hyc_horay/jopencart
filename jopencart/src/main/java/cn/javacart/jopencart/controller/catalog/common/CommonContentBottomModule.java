package cn.javacart.jopencart.controller.catalog.common;

import java.util.ArrayList;
import java.util.List;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;
import cn.javacart.jopencart.model.LayoutModule;
import cn.javacart.jopencart.service.catalog.design.DesignLayoutService;

import com.jfinal.kit.StrKit;
import com.jfinal.plugin.activerecord.Db;
import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 内容底部
 * @author farmer
 *
 */
public class CommonContentBottomModule extends JOpencartModule{

	public CommonContentBottomModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		Integer layoutId = DesignLayoutService.ME.getLayout(route);
		if(layoutId == null){
			layoutId =Integer.parseInt((String)config.get("config_layout_id"));
		}
		List<String> modules = new ArrayList<String>();
		List<LayoutModule> layoutModules = DesignLayoutService.ME.getLayoutModules(layoutId, "content_bottom");		
		for (LayoutModule layoutModule : layoutModules) {
			String code = layoutModule.getStr("code");
			if("0".equals(code)){
				continue;
			}
			String[] codes = code.split("[.]");
			if(codes != null){
				if(config.get(codes[0]+"_status") != null){
					modules.add(load.module("cn.javacart.jopencart.controller.catalog.module.Module"+StrKit.firstCharToUpperCase(codes[0])+"Module",controller));
				}else if(codes[1] != null){
					String setting = Db.queryStr("select setting from joc_module t where t.module_id = ?" , codes[1]);
					modules.add(load.module("cn.javacart.jopencart.controller.catalog.module.Module"+StrKit.firstCharToUpperCase(codes[0])+"Module",controller,setting));
				}
			}
		}
		dataMap.put("modules", MemoryUtils.valueOf(modules));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/content_bottom.tpl", dataMap));
	}

}
