package cn.javacart.jopencart.controller.catalog.common;

import org.joda.time.DateTime;

import php.runtime.memory.support.MemoryUtils;
import cn.javacart.jfinal.php.render.PhpRender;
import cn.javacart.jopencart.engine.JOpencartController;
import cn.javacart.jopencart.engine.JOpencartModule;

import com.jfplugin.kit.mockrender.MockRenderKit;

/**
 * 页脚
 * @author farmer
 *
 */
public class CommonFooterModule extends JOpencartModule{
	
	public CommonFooterModule(JOpencartController controller) {
		super(controller);
	}

	@Override
	public String render() {
		/*加载语言*/
		language.load("/catalog/language/{0}/common/footer.php");
		dataMap.put("text_information", language.get("text_information"));
		dataMap.put("text_service", language.get("text_service"));
		dataMap.put("text_extra", language.get("text_extra"));
		dataMap.put("text_contact", language.get("text_contact"));
		dataMap.put("text_return", language.get("text_return"));
		dataMap.put("text_sitemap", language.get("text_sitemap"));
		dataMap.put("text_manufacturer", language.get("text_manufacturer"));
		dataMap.put("text_voucher", language.get("text_voucher"));
		dataMap.put("text_affiliate", language.get("text_affiliate"));
		dataMap.put("text_special", language.get("text_special"));
		dataMap.put("text_account", language.get("text_account"));
		dataMap.put("text_order", language.get("text_order"));
		dataMap.put("text_wishlist", language.get("text_wishlist"));
		dataMap.put("text_newsletter", language.get("text_newsletter"));
		dataMap.put("powered", MemoryUtils.valueOf(String.format(language.getStr("text_powered"),config.get("config_name"), DateTime.now().getYear())));
		return MockRenderKit.render(new PhpRender("/catalog/view/theme/default/template/common/footer.tpl", dataMap));
	}

}
